<?php

/**
 * Created by PhpStorm.
 * User: gerard
 * Date: 09/09/2014
 * Time: 10:31
 */

namespace MoocBundle\Form\Handler;

use Sonata\UserBundle\Form\Handler\ProfileFormHandler as BaseHandler;

class ProfileFormHandler extends BaseHandler {

    protected $form;
    protected $request;
    protected $templating;

    public function __construct($request, $form, $templating) {
        $this->request = $request;
        $this->form = $form;
        $this->templating = $templating;
    }

}
