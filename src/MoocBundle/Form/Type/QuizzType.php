<?php

namespace MoocBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use MoocBundle\Form\Type\QuestionCreateType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuizzType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
           
            ->add('level',NULL,array('required' => true))
            ->add('title',NULL,array('required' => true))
            ->add('type', 'choice', array(
   
    'choices'  => array('TIMED' => 'Timed', 'NOT TIMED' => 'Not Timed'),'required'=>true
   
  
))
       
            ->add('skill','entity',array('class'=>'MoocBundle:Skill','property'=>'name','required'=>true))
 
             
            ->add('questions', 'collection', array(

        'type'         => new QuestionCreateType(),

        'allow_add'    => true,

        'allow_delete' => true,
        
      

      ))

      ->add('save',      'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MoocBundle\Entity\Quizz'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'moocbundle_quizz';
    }
}
