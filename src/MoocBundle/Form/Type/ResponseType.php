<?php

namespace MoocBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResponseType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('statement')
                ->add('type', 'choice', array(
                    'choices' => array('CORRECT' => 'Correct', 'INCORRECT' => 'Incorrect'), 'required' => true
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'MoocBundle\Entity\Response'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'moocbundle_response';
    }

}
