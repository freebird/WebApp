<?php

namespace MoocBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use MoocBundle\Entity\Question;
use MoocBundle\Entity\Response;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuestionType
 *
 * @author Rim
 */
class QuestionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
 $entity = $builder->getData();
       $builder
                ->add('statement');
        /*    $builder->add('responses', 'entity', array(

          'class' => 'MoocBundle:Response',

          'query_builder' => function(EntityRepository $er)  use($options)
          {
          return $er->createQueryBuilder('u')
          ->where('u.question = :id')
          ->setParameter('id', true);

          },
          'property' => 'statement',
          'expanded' => true ,
          'multiple'=>true
          )) */

        $builder
                ->add('responses', 'entity', array(
                    
                    'class' => 'MoocBundle:Response',
                     'choices' => $entity->getResponses(),
                   

                    'property' => 'statement',
                    'expanded' => true,
                    'multiple' => true)
                )
                  ->add('next','submit', array(
    'attr' => array('class' => 'next')));
                
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'MoocBundle\Entity\Question'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'question';
    }

    //put your code here
}
