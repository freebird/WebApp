<?php

namespace MoocBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use MoocBundle\Form\Type\ResponseType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuestionCreateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('level')
            ->add('statement')
            ->add('responses', 'collection', array(

        'type'         => new ResponseType(),

        'allow_add'    => true,

        'allow_delete' => true,
         
        'prototype_name' => '__nameR__',
        
      

      ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MoocBundle\Entity\Question'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'moocbundle_question';
    }
}
