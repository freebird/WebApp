<?php

namespace MoocBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use MoocBundle\Form\MediaType;

class CourseType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title')
                ->add('level')
                ->add('duration')
                ->add('description')
                ->add('book')
                ->add('available')
                ->add('published')
                ->add('rate')
                ->add('video', new VideoType())
                ->add('Icon', new MediaType())
                ->add('instructor', 'hidden')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'MoocBundle\Entity\Course'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'moocbundle_course';
    }

}
