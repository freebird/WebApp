<?php

namespace MoocBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MoocBundle extends Bundle {

    /**
     * {@inheritdoc}
     */
    public function getParent() {
        return 'SonataUserBundle';
    }

}
