<?php

namespace MoocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class QuizzAdmin extends Admin {

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'Quizz';

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('title')
                ->add('type', 'choice', array(
                    'choices' => array('TIMED' => 'Timed', 'NOT TIMED' => 'Not Timed')
                ))
                ->add('level')
                ->add('duration')
                ->add('Skill', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Skill',
                    'property' => 'name'))
                ->add('chapter', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Chapter',
                    'property' => 'title'));
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('type')
                ->add('level')
                ->add('title')
                ->add('duration')
                ->add('skill')
                ->add('chapter');
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title', null, array('label' => 'Title'))
                ->add('level', null, array('label' => 'Level'))
                ->add('type', null, array('label' => 'Type'))
                ->add('duration')
                ->add('skill.name', null, array('label' => 'Skill'))
                ->add('chapter.title', null, array('label' => 'Chapter'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))



        ;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('type')
                ->add('level')
                ->add('duration')
                ->add('skill.name')
                ->add('chapter')
        ;
    }

}
