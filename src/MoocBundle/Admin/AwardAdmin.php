<?php

namespace MoocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AwardAdmin extends Admin {

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'Awards';

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('type')
                ->add('level')
                ->add('icon')
                ->add('Skill', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Skill',
                    'property' => 'name'))
                ->add('dicipline')
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('type')
                ->add('level')
                ->add('icon')
                ->add('dicipline');
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
               
                ->addIdentifier('type', null, array('label' => 'Type'))
                ->add('level', null, array('label' => 'Level'))
                ->add('icon', null, array('label' => 'Icon'))
                ->add('skill.name', null, array('label' => 'Skill'))
                ->add('dicipline', null, array('label' => 'Dicipline'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                       'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('type')
                ->add('level')
                ->add('icon')
                ->add('skill.name')
                ->add('dicipline')
        ;
    }

}
