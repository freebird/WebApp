<?php

namespace MoocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CourseAdmin extends Admin {

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'Courses';

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('General', array('class' => 'col-md-9'))
                ->add('title', 'text')
                ->add('level', 'number')
                ->add('duration')
                ->add('description')
                ->add('book', 'text')
                ->add('published', 'date')
                ->add('available', 'checkbox', array(
                    'label' => 'Available',
                    'required' => false,
                ))
                ->add('instructor', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\User',
                    'property' => 'lastName'))
                ->end()
                ->with('Media', array('class' => 'col-md-3'))
                ->add('video', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Video',
                    'property' => 'name',
                ))
                ->add('icon', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Media',
                    'property' => 'name'
                ))
                ->end()
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title')
                ->add('level')
                ->add('duration')
                ->add('description')
                ->add('book')
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title', null, array('label' => 'Title'))
                ->add('level', null, array('label' => 'Level'))
                ->add('duration', null, array('label' => 'Duration'))
                ->add('instructor.lastName', null, array('label' => 'Instructor'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('level')
                ->add('instructor')
                ->add('book')
                ->add('video')
        ;
    }

}
