<?php

namespace MoocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class VideoAdmin extends Admin {

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'Video';

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('name')
                ->add('url')
                ->add('tag1')
                ->add('tag2')
                ->add('tag3')
                ->add('chapter')
                ->add('chapter', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Chapter',
                    'property' => 'title'))
                ->add('course', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Course',
                    'property' => 'title'))


        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
                ->add('url')
                ->add('tag1')
                ->add('tag2')
                ->add('tag3')
                ->add('chapter');
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('name', null, array('label' => 'Name'))
                ->add('url', null, array('label' => 'Url'))
                ->add('tag1', null, array('label' => 'Tag1'))
                ->add('tag2', null, array('label' => 'Tag2'))
                ->add('tag3', null, array('label' => 'Tag3'))
                ->add('chapter.title', null, array('label' => 'Chapter'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))





        ;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('name')
                ->add('url')
                ->add('tag1')
                ->add('tag2')
                ->add('tag3')
                ->add('chapter')
                ->add('course')
        ;
    }

}
