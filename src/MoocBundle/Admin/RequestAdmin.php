<?php

namespace MoocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class RequestAdmin extends Admin {

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'Request';

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('instructor', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\User',
                    'property' => 'lastName'))
                ->add('organism', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\User',
                    'property' => 'name'))
                ->add('type')
                ->add('status', 'choice', array(
                    'choices' => array('1' => 'Accepted', '0' => 'Waiting', '-1' => 'Ignored')
                ))
                ->add('date')
                ->add('content')

        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('instructor')
                ->add('organism')
                ->add('type')
                ->add('status')
                ->add('date')
                ->add('content');
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('content', null, array('label' => 'Content'))
                ->add('date', null, array('label' => 'Date'))
                ->add('status', null, array('label' => 'Status'))
                ->add('type', null, array('label' => 'Type'))
                ->add('organism.name', null, array('label' => 'Organisme'))
                ->add('instructor.lastName', null, array('label' => 'Instructor'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))



        ;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('instructor')
                ->add('organism')
                ->add('type')
                ->add('status')
                ->add('date')
                ->add('content')
        ;
    }

}
