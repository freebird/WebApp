<?php

namespace MoocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ChapterAdmin extends Admin {

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'Chapter';

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('title')
                ->add('description')
                ->add('video')
                ->add('content')
                ->add('available', 'checkbox', array(
                    'label' => 'Available',
                    'required' => false,
                ))
                ->add('presentation')
                ->add('course', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Course',
                    'property' => 'title'))


        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('title')
                ->add('description')
                ->add('video')
                ->add('content')
                ->add('available')
                ->add('presentation')
                ->add('course');
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title', null, array('label' => 'Title'))
                ->add('description', null, array('label' => 'Description'))
                //->add('video', null, array('label' => 'Video'))
                //     ->add('Content', null, array('label' => 'Content'))
                ->add('available', null, array('label' => 'available'))
                //  ->add('presentation', null, array('label' => 'Presentation'))
                ->add('course.title', null, array('label' => 'Course'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('title')
                ->add('description')
                ->add('video')
                ->add('content')
                ->add('available')
                ->add('presentation')
                ->add('course')
        ;
    }

}
