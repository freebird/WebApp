<?php

namespace MoocBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class SubscriptionAdmin extends Admin {

    /**
     * {@inheritdoc}
     */
    protected $baseRoutePattern = 'Subscription';

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('date')
                ->add('type')
                ->add('student', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\User',
                    'property' => 'lastName'))
                ->add('instructor', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\User',
                    'property' => 'lastName'))
                ->add('course', 'sonata_type_model', array(
                    'class' => 'MoocBundle\Entity\Course',
                    'property' => 'title'))

        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     *
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('date')
                ->add('type')
                ->add('course')
                ->add('student')
                ->add('instructor');
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('type', null, array('label' => 'Type'))
                ->add('date', null, array('label' => 'Date'))
                ->add('course.title', null, array('label' => 'Course'))
                ->add('student.lastName', null, array('label' => 'Student'))
                ->add('instructor.lastName', null, array('label' => 'Instructor'))
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))




        ;
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $showMapper
                ->add('date')
                ->add('type')
                ->add('course')
                ->add('student')
                ->add('instructor')
        ;
    }

}
