<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Video
 *
 * @ORM\Table(name="video")
 * @ORM\Entity
 */
class Video {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="URL", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="RATE", type="integer", nullable=true)
     */
    private $rate;

    /**
     * @var string
     *
     * @ORM\Column(name="TAG1", type="string", length=255, nullable=true)
     */
    private $tag1;

    /**
     * @var string
     *
     * @ORM\Column(name="TAG2", type="string", length=255, nullable=true)
     */
    private $tag2;

    /**
     * @var string
     *
     * @ORM\Column(name="TAG3", type="string", length=255, nullable=true)
     */
    private $tag3;

    /**
     * @var Chapter
     *
     * @ORM\ManyToOne(targetEntity="Chapter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CHAPTER", referencedColumnName="id")
     * })
     */
    private $chapter;
    
    /**
     * @var Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="COURSE", referencedColumnName="id")
     * })
     */
    private $course;
   

    function getCourse() {
        return $this->course;
    }

    function setCourse(Course $course) {
        $this->course = $course;
    }

    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getUrl() {
        return $this->url;
    }

    function getRate() {
        return $this->rate;
    }

    function getTag1() {
        return $this->tag1;
    }

    function getTag2() {
        return $this->tag2;
    }

    function getTag3() {
        return $this->tag3;
    }

    function getChapter() {
        return $this->chapter;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function setRate($rate) {
        $this->rate = $rate;
    }

    function setTag1($tag1) {
        $this->tag1 = $tag1;
    }

    function setTag2($tag2) {
        $this->tag2 = $tag2;
    }

    function setTag3($tag3) {
        $this->tag3 = $tag3;
    }

    function setChapter(Chapter $chapter) {
        $this->chapter = $chapter;
    }

    public function __toString() {
        return "video";
    }

}
