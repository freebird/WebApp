<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mark
 *
 * @ORM\Table(name="mark")
 * @ORM\Entity
 */
class Mark {

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="MARK", type="float", precision=10, scale=0, nullable=true)
     */
    private $mark;

    /**
     * @var \Quizz
     *
     * @ORM\ManyToOne(targetEntity="Quizz")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="QUIZZ", referencedColumnName="id")
     * })
     */
    private $quizz;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="USER", referencedColumnName="id")
     * })
     */
    private $user;

    function getId() {
        return $this->id;
    }

    function getMark() {
        return $this->mark;
    }

    function getquizz() {
        return $this->quizz;
    }

    function getuser() {
        return $this->user;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setMark($mark) {
        $this->mark = $mark;
    }

    function setquizz( $quizz) {
        $this->quizz = $quizz;
    }

    function setIduser(\User $user) {
        $this->user = $user;
   }

    public function __toString() {
        return "mark";
    }

}
