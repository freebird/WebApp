<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MoocBundle\Entity\Question;

/**
 * Response
 *
 * @ORM\Table(name="response")
 * @ORM\Entity
 */
class Response
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="STATEMENT", type="string", length=255, nullable=true)
     */
    private $statement;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="responses")
     */
    private $question;

    function getId() {
        return $this->id;
    }

    function getStatement() {
        return $this->statement;
    }

    function getType() {
        return $this->type;
    }

    function getQuestion() {
        return $this->question;
    }

    
    function setId($id) {
        $this->id = $id;
    }

    function setStatement($statement) {
        $this->statement = $statement;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setQuestion($question) {
        $this->question = $question;
    }

    
    public function __toString() {
                return 'Response';

    }


}
