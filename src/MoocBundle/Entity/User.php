<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="MoocBundle\Entity\Repository\UserRepository")
 */
class User extends BaseUser {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     * @ORM\JoinTable(name="users_groups")
     */
    protected $groups;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BIRTHDAY", type="date", nullable=true)
     */
    protected $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="AVATAR", type="text", nullable=true)
     */
    protected $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="ROLE", type="string", length=20, nullable=true)
     */
    protected $role;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=20, nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIVITY", type="string", length=20, nullable=true)
     */
    protected $activity;

    /**
     * @var string
     *
     * @ORM\Column(name="FAX", type="string", length=20, nullable=true)
     */
    protected $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="ADDRESS", type="string", length=50, nullable=true)
     */
    protected $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="EXPERIENCE", type="integer", nullable=true)
     */
    protected $experience;

    /**
     * @var string
     *
     * @ORM\Column(name="SPECIALTY", type="string", length=20, nullable=true)
     */
    protected $specialty;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="RATE", type="integer", nullable=true)
     */
    protected $rate;
    
    protected $zipCode;
    
    protected $roles;

    function getZipCode() {
        return $this->zipCode;
    }

    function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }
    
    function getRate() {
        return $this->rate;
    }

    function setRate($rate) {
        $this->rate = $rate;
    }

    
    public function __construct() {
        parent::__construct();
        $this->groups = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getBirthday() {
        return $this->birthday;
    }

    function getPassword() {
        return $this->password;
    }

    function getAvatar() {
        return $this->avatar;
    }

    function getRole() {
        return $this->role;
    }

    function getName() {
        return $this->name;
    }

    function getActivity() {
        return $this->activity;
    }

    function getPhone() {
        return $this->phone;
    }

    function getFax() {
        return $this->fax;
    }

    function getAddress() {
        return $this->address;
    }

    function getExperience() {
        return $this->experience;
    }

    function getSpecialty() {
        return $this->specialty;
    }

    function getFirstname() {
        return $this->firstname;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getDescription() {
        return $this->description;
    }

    function getExpiresAt() {
        return $this->expiresAt;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setBirthday(\DateTime $birthday) {
        $this->birthday = $birthday;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    function setRole($role) {
        $this->role = $role;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setActivity($activity) {
        $this->activity = $activity;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setFax($fax) {
        $this->fax = $fax;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function setExperience($experience) {
        $this->experience = $experience;
    }

    function setSpecialty($specialty) {
        $this->specialty = $specialty;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    public function __toString() {
        return "user";
    }

    
}
