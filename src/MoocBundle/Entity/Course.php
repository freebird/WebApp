<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MoocBundle\Entity\User;

/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity(repositoryClass="MoocBundle\Entity\Repository\CourseRepository")
 */
class Course {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TITLE", type="string", length=20, nullable=true)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="LEVEL", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DURATION", type="date", nullable=true)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="MoocBundle\Entity\Video", cascade={"persist"})
     *
     */
    private $video;

    /**
     * @ORM\OneToOne(targetEntity="MoocBundle\Entity\Media", cascade={"persist"})
     *
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="BOOK", type="text", nullable=true)
     */
    private $book;

    /**
     * @var boolean
     *
     * @ORM\Column(name="AVAILABLE", type="boolean", nullable=true)
     */
    private $available;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="PUBLISHED", type="date", nullable=true)
     */
    private $published;

    /**
     * @var string
     *
     * @ORM\Column(name="RATE", type="decimal", precision=4, scale=2, nullable=true)
     */
    private $rate;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="INSTRUCTOR", referencedColumnName="id")
     * })
     */
    private $instructor;

    function __construct() {
        $this->instructor = null;
    }

    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getLevel() {
        return $this->level;
    }

    function getDuration() {
        return $this->duration;
    }

    function getDescription() {
        return $this->description;
    }

    function getVideo() {
        return $this->video;
    }

    function getBook() {
        return $this->book;
    }

    function getAvailable() {
        return $this->available;
    }

    function getPublished() {
        return $this->published;
    }

    function getRate() {
        return $this->rate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setLevel($level) {
        $this->level = $level;
    }

    function setDuration(\DateTime $duration) {
        $this->duration = $duration;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setVideo($video) {
        $this->video = $video;
    }

    function setBook($book) {
        $this->book = $book;
    }

    function setAvailable($available) {
        $this->available = $available;
    }

    function setPublished(\DateTime $published) {
        $this->published = $published;
    }

    function setRate($rate) {
        $this->rate = $rate;
    }

    function getIcon() {
        return $this->icon;
    }

    function getInstructor() {
        return $this->instructor;
    }

    function setIcon($icon) {
        $this->icon = $icon;
    }

    function setInstructor(User $instructor) {
        $this->instructor = $instructor;
    }

    public function __toString() {
        return "Course";
    }

}
