<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MoocBundle\Entity\Course;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Chapter
 *
 * @ORM\Table(name="chapter")
 * @ORM\Entity
 */
class Chapter {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TITLE", type="string", length=50, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="VIDEO", type="text", nullable=true)
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=true)
     */
    private $content;

    /**
     *
     * @ORM\Column(name="AVAILABLE", type="boolean", nullable=true)
     */
    private $available;

    /**
     * @var string
     *
     * @ORM\Column(name="PRESENTATION", type="text", nullable=false)
     */
    private $presentation;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course", referencedColumnName="id")
     * })
     */
    private $course;

    function __construct() {
        $this->course = new Course();
    }

    function getId() {
        return $this->id;
    }

    function getTitle() {
        return $this->title;
    }

    function getDescription() {
        return $this->description;
    }

    function getVideo() {
        return $this->video;
    }

    function getContent() {
        return $this->content;
    }

    function getAvailable() {
        return $this->available;
    }

    function getPresentation() {
        return $this->presentation;
    }

    function getCourse() {
        return $this->course;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setVideo($video) {
        $this->video = $video;
    }

    function setContent($content) {
        $this->content = $content;
    }

    function setAvailable($available) {
        $this->available = $available;
    }

    function setPresentation($presentation) {
        $this->presentation = $presentation;
    }

    function setCourse(Course $course) {
        $this->course = $course;
    }

    public function __toString() {
        return "chapitre";
    }

}
