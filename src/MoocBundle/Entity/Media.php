<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Media
 *
 * @ORM\Table("media")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Media {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * 
     * @ORM\COlumn(name="updated_at",type="datetime", nullable=true) 
     */
    private $updateAt;

    /**
     * @ORM\PostLoad()
     */
    public function postLoad() {
        $this->updateAt = new \DateTime();
    }

    /**
     * @ORM\Column(type="string",length=255) 
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string",length=255, nullable=true) 
     */
    private $path;
    public $file;

    public function setName($name) {
        $this->name = $name;
    }

    public function setPath($path) {
        $this->path = $path;
    }

    public function getUploadRootDir() {
        return __dir__ . '/../../../web/uploads/icon';
    }

    public function getAbsolutePath() {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getAssetPath() {
        return 'uploads/icon' . $this->name;
    }

    /**
     * @ORM\Prepersist()
     * @ORM\Preupdate() 
     */
    public function preUpload() {
        $this->tempFile = $this->getAbsolutePath();
        $this->oldFile = $this->getPath();
        $this->updateAt = new \DateTime();
        var_dump($this->file->getClientOriginalName());
        var_dump($this->getUploadRootDir());

        if (null !== $this->file) {
            $this->path = $this->file->getClientOriginalName();
            $this->name = $this->file->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate() 
     */
    public function upload() {
        
        if (null !== $this->file) {
            $this->file->move($this->getUploadRootDir(), $this->name);
            unset($this->file);

            if ($this->oldFile != null)
                unlink($this->tempFile);
        }
    }

    /**
     * @ORM\PreRemove() 
     */
    public function preRemoveUpload() {
        $this->tempFile = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove() 
     */
    public function removeUpload() {
        if (file_exists($this->tempFile))
            unlink($this->tempFile);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function getPath() {
        return $this->path;
    }

    public function getName() {
        
        return $this->name;
    }

    function getUpdateAt() {
        return $this->updateAt;
    }

    function setUpdateAt(\DateTime $updateAt) {
        $this->updateAt = $updateAt;
    }

    public function __toString() {
        return "Media";
    }

}
