<?php

namespace MoocBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use MoocBundle\Entity\Quizz;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity
 */
class Question {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="LEVEL", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="STATEMENT", type="string", length=255, nullable=true)
     */
    private $statement;

    /**
     * @ORM\ManyToOne(targetEntity="Quizz", inversedBy="questions")
     */
    private $quizz;

    /**
     * @ORM\OneToMany(targetEntity="Response", mappedBy="question" , cascade={"persist"})
     */
    private $responses;
    
    function setResponses($responses) {
        $this->responses = $responses;
    }

        function getId() {
        return $this->id;
    }

    function getLevel() {
        return $this->level;
    }

    function getStatement() {
        return $this->statement;
    }

    function getQuizz() {
        return $this->quizz;
    }

    function getResponses() {
        return $this->responses;
    }

    function __construct() {
        $this->responses = new ArrayCollection();
    }

    function setStatement($statement) {
        $this->statement = $statement;
    }

    function setQuizz($quizz) {
        $this->quizz = $quizz;
    }

    function setLevel($level) {
        $this->level = $level;
    }

    public function __toString() {
        return 'Question';
    }

    function getCorrectResponses()
    { $result = new ArrayCollection();
    
    $responses = $this->getResponses();
    foreach ($responses as $value)
    {if ($value->getType()=="CORRECT")
    { $result->add($value->getStatement()) ;}
        
        
    }
    return $result ;
    }
    
    public function setResponseQuestion()
    {$responses=$this->getResponses();
         foreach ($responses as $response)
         {
             $response->setQuestion($this);
         }
    }

}
