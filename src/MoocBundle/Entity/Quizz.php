<?php

namespace MoocBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use MoocBundle\Entity\Skill;
use MoocBundle\Entity\Chapter;
use Doctrine\ORM\Mapping as ORM;

/**
 * Quizz
 *
 * @ORM\Table(name="quizz")
 * @ORM\Entity
 */
class Quizz {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=20, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="DURATION", type="integer")
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="LEVEL", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="TITLE", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var \Skill
     *
     * @ORM\ManyToOne(targetEntity="Skill")
     * @ORM\JoinColumn(name="SKILL", referencedColumnName="id")
     */
    private $skill;

    /**
     * @var \Chapter
     *
     * @ORM\ManyToOne(targetEntity="Chapter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CHAPTER", referencedColumnName="id")
     * })
     */
    private $chapter;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="quizz" , cascade={"persist"})
     */
    private $questions;

    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function getLevel() {
        return $this->level;
    }

    function getTitle() {
        return $this->title;
    }

    function getSkill() {
        return $this->skill;
    }

    function setSkill(Skill $skill) {
        $this->skill = $skill;
    }

    function getChapter() {
        return $this->chapter;
    }

    function getQuestions() {
        return $this->questions;
    }

    function getDuration() {
        return $this->duration;
    }

    function setDuration($duration) {
        $this->duration = $duration;
    }

    function __construct() {
        $this->questions = new ArrayCollection();
   
    }

    function setChapter(Chapter $chapter) {
        $this->chapter = $chapter;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setLevel($level) {
        $this->level = $level;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setQuestions($questions) {
        $this->questions = $questions;
    }

    public function __toString() {
        return "quizz";
    }
    public function setQuestionId()
    {$questions=$this->getQuestions();
         foreach ($questions as $question)
         {
             $question->setQuizz($this);
         }
    }

}
