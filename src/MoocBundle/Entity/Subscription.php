<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Subscription
 *
 * @ORM\Table(name="subscription")
 * @ORM\Entity
 */
class Subscription {

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="DATE", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=10, nullable=true)
     */
    private $type;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="COURSE", referencedColumnName="id")
     * })
     */
    private $course;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STUDENT", referencedColumnName="id")
     * })
     */
    private $student;

    /**
     * @var \User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="INSTRUCTOR", referencedColumnName="id")
     * })
     */
    private $instructor;
    
    
    function __construct() {
        $this->date = new \DateTime('now');
    }

    
    function getId() {
        return $this->id;
    }

    function getDate() {
        return $this->date;
    }

    function getType() {
        return $this->type;
    }

    function getIdcourse() {
        return $this->idcourse;
    }

    function getStudent() {
        return $this->student;
    }

    function getInstructor() {
        return $this->instructor;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDate(\DateTime $date) {
        $this->date = $date;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setCourse(Course $course) {
        $this->course = $course;
    }

    function setStudent(User $student) {
        $this->student = $student;
    }

    function setInstructor(User $instructor) {
        $this->instructor = $instructor;
    }
    
    function getCourse() {
        return $this->course;
    }

    
    public function __toString() {
        return "subscription";
    }

}
