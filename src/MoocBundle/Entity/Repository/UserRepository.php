<?php
namespace MoocBundle\Entity\Repository;
use Doctrine\ORM\EntityRepository;


class UserRepository extends EntityRepository {
    function recherOrg($name)
   {
$query = $this->createQueryBuilder('a')
               ->where('a.name LIKE :name')
               ->andWhere('a.role=:role')
               ->setParameter('name', '%'.$name.'%')
                ->setParameter('role',"ORGANISM")
              ->getQuery();
return $query->getResult();
   } 
   function recherIns($name)
   {
$query = $this->createQueryBuilder('a')
               ->where('a.lastname LIKE :lastname')
               ->andWhere('a.role=:role')
               ->setParameter('lastname', '%'.$name.'%')
                ->setParameter('role',"INSTRUCTOR")
              ->getQuery();
return $query->getResult();
   } 
}
