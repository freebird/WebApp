<?php

namespace MoocBundle\Entity\Repository;
use Doctrine\ORM\EntityRepository;

class CourseRepository extends EntityRepository {
   function recherTitle($title)
   {
$query = $this->createQueryBuilder('a')
               ->where('a.title LIKE :title')
               ->setParameter('title', '%'.$title.'%')
              ->getQuery();
return $query->getResult();
   }
}
