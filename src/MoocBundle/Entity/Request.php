<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Request
 *
 * @ORM\Table(name="request")
 * @ORM\Entity
 */
class Request {

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="INSTRUCTOR", referencedColumnName="id")
     * })
     */
    private $instructor;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORGANISM", referencedColumnName="id")
     * })
     */
    private $organism;

    /**
     * @var integer
     *
     * @ORM\Column(name="TYPE", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="STATUS", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENT", type="text", nullable=true)
     */
    private $content;

    function getId() {
        return $this->id;
    }

    function getInstructor() {
        return $this->instructor;
    }

    function getOrganism() {
        return $this->organism;
    }

    function setInstructor(\User $instructor) {
        $this->instructor = $instructor;
    }

    function setOrganism(\User $organism) {
        $this->organism = $organism;
    }

    function getType() {
        return $this->type;
    }

    function getStatus() {
        return $this->status;
    }

    function getDate() {
        return $this->date;
    }

    function getContent() {
        return $this->content;
    }

    function setId($id) {
        $this->id = $id;
    }


    function setType($type) {
        $this->type = $type;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setDate(\DateTime $date) {
        $this->date = $date;
    }

    function setContent($content) {
        $this->content = $content;
    }

    public function __toString() {
        return "request";
    }

}
