<?php

namespace MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Award
 *
 * @ORM\Table(name="award", indexes={@ORM\Index(name="fk_idstudent", columns={"STUDENT"})})
 * @ORM\Entity
 */
class Award {

    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPE", type="string", length=30, nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="LEVEL", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="ICON", type="text", nullable=true)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="DICIPLINE", type="string", length=30, nullable=true)
     */
    private $dicipline;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="STUDENT", referencedColumnName="id")
     * })
     */
    private $student;

    /**
     * @var \Quizz
     *
     * @ORM\ManyToOne(targetEntity="Quizz")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="QUIZZ", referencedColumnName="id")
     * })
     */
    private $quizz;

    /**
     * @var \Skill
     *
     * @ORM\ManyToOne(targetEntity="Skill")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="SKILL", referencedColumnName="id")
     * })
     */
    private $skill;

    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function getLevel() {
        return $this->level;
    }

    function getIcon() {
        return $this->icon;
    }

    function getDicipline() {
        return $this->dicipline;
    }

    function getStudent() {
        return $this->student;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setLevel($level) {
        $this->level = $level;
    }

    function setIcon($icon) {
        $this->icon = $icon;
    }

    function setDicipline($dicipline) {
        $this->dicipline = $dicipline;
    }

    function getSkill() {
        return $this->skill;
    }

    function setSkill(Skill $skill) {
        $this->skill = $skill;
    }

    public function __toString() {
        return "award";
    }

}
