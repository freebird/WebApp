<?php

namespace MoocBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Symfony\Component\HttpFoundation\Response;
use MoocBundle\Entity\Mailinglist;

/**
 * Description of MailinglistController
 *
 * @author Asus
 */
class MailinglistController extends Controller {

    function ajouterAction() {
        $requete = $this->get('request');
        // var_dump($requete);
        //le contenu d'une requete http

        $email = $requete->get('email');


        if ($email != '') {
            $mailinglist = new Mailinglist();
            $mailinglist->setEmail($email);


            $em = $this->getDoctrine()->getManager();
            $em->persist($mailinglist);
            $em->flush();

            return $this->redirectToRoute('mailing_confirmed');
        }

        return $this->render('MoocBundle:mailinglist:ajoutermailinglist.html.twig');
    }



    
     public function afficherAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MoocBundle:Mailinglist')->findAll();

        return $this->render('MoocBundle:Mailinglist:affichermailinglist.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    function confirmedAction() {
        $em = $this->getDoctrine()->getManager();

        return $this->render('MoocBundle:Mailinglist:mailingconfirmed.html.twig');
    }

    function supprimerAction($id) {
        $em = $this->getDoctrine()->getManager();
        $mailinglist = $em->getRepository('MoocBundle:Mailinglist')->findOneByEmail($id);

        $em->remove($mailinglist);
        $em->flush();
        return $this->redirectToRoute('afficher_mailinglist');
    }

}
