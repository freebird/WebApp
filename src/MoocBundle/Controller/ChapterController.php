<?php
namespace MoocBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MoocBundle\Entity\Chapter;
use MoocBundle\Entity\Course;
use MoocBundle\Entity\Quizz;
use MoocBundle\Form\Type\ChapterType;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChapterController
 *
 * @author Rim
 */
class ChapterController extends Controller{
    
    
    
   public function createAction(Request $request,$id) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Chapter();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $course = $em->getRepository('MoocBundle:Course')->findOneById($id);
        if ($form->isValid()) {
            
            
            $entity->setCourse($course);
            $em->persist($entity);
            $em->flush();

         return $this->redirect($this->generateUrl('course_instructor_details', array('id' =>$id)));
        }

        return $this->render('MoocBundle:Chapter:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }
    private function createCreateForm($entity) {
        $form = $this->createForm(new ChapterType(), $entity);

        

        
        return $form;
    }
    
    
    public function showAction($id) {

        $em1 = $this->getDoctrine()->getManager();
        $chapter = $em1->getRepository('MoocBundle:Chapter')->findOneById($id);
        $quizz=$em1->getRepository('MoocBundle:Quizz')->findBy(array('chapter' => $id));
        return $this->render('MoocBundle:Chapter:show.html.twig', array('chapter' => $chapter,'quizz'=>$quizz));
    }
}
