<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuizController
 *
 * @author Rim
 */

namespace MoocBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use MoocBundle\Entity\Quizz;
use MoocBundle\Entity\Mark;
use MoocBundle\Entity\Question;
use MoocBundle\Form\Type\QuestionType;
use MoocBundle\Form\Type\QuizzType;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\HttpFoundation\JsonResponse;

class QuizController extends Controller {

    public function takeQuizAction(Request $request, $id, $i) {
         $em = $this->getDoctrine()->getManager();
       
        
        $Corrrectresponses = new ArrayCollection();

       
        $quiz = $em->getRepository('MoocBundle:Quizz')->findOneById($id);

        $question = $quiz->getQuestions()->get($i);

        $form = $this->createCreateForm($question);


        if (isset($_POST['recup'])) {
         echo($i);
            $quiz1 = $em->getRepository('MoocBundle:Quizz')->findOneById($id);
            $CorrectResponses = $quiz1->getQuestions()->get($i - 1)->getCorrectResponses()->getValues();
            var_dump($CorrectResponses);
            var_dump($_POST['recup']);
            if ($_POST['recup'] == $CorrectResponses) {
              $this->updateMark(1);   // IdUser
            }
        }
 if ($i>0) {
     return $this->render('MoocBundle:Quiz:form.html.twig', array('form' => $form->createView(), 'i' => $i , 'id' => $id));
 }
        return $this->render('MoocBundle:Quiz:takeQuiz.html.twig', array('form' => $form->createView(), 'i' => $i, 'id' => $id));
    }

    private function createCreateForm(Question $question) {
        $form = $this->createForm(new QuestionType(), $question); // id quiz 

        return $form;
    }
    
    private function updateMark($id) // id User
    {
        $em = $this->getDoctrine()->getManager();
        $mark = $em->getRepository('MoocBundle:Mark')->findOneById($id); // idUser
        if ($mark == NULL)
        {
         $mark= new Mark();
       // $mark->setId(1); // IdUser
        $em->persist($mark);
        $em->flush();
        $mark->setMark($mark->getMark()+1);
      
        }
        $mark->setMark($mark->getMark()+1);
          $em->persist($mark);
        $em->flush();
        
        
        
        
    }
    public function addQuizAction(Request $request)

  {

    $quiz = new Quizz();

    $form = $this->createForm(new QuizzType(), $quiz);


    if ($form->handleRequest($request)->isValid()) {

      $em = $this->getDoctrine()->getManager();
      $chapter=$em->getRepository('MoocBundle:Chapter')->findOneById(2);
      $quiz->setChapter($chapter);
      $quiz->setQuestionId();
      $questions=$quiz->getQuestions();
         foreach ($questions as $question)
         {
             $question->setResponseQuestion();
         }
      $em->persist($quiz);

      $em->flush();


      $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');


      

    }


    return $this->render('MoocBundle:Quiz:add.html.twig', array(

      'form' => $form->createView(),

    ));

  }

}
