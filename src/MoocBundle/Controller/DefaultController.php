<?php

namespace MoocBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

    public function indexAction() {


        return $this->render('MoocBundle:Default:index.html.twig');
    }

    public function displayCoursesAction() {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository('MoocBundle:Course')->findAll();

        return $this->render('MoocBundle:Default:topcourses.html.twig', array('result' => $result));
    }

    public function progressBarAction() {

        $repo = $this->getDoctrine()
                ->getManager()
                ->getRepository('MoocBundle:Course');

        $qb = $repo->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $qb->where('a.available = :av');
        $qb->setParameter('av', 1);

        $countCourses = $qb->getQuery()->getSingleScalarResult();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('MoocBundle:User');
        $query = $repository->createQueryBuilder('u')
                ->select('COUNT(u)')
                ->innerJoin('u.groups', 'g')
                ->where('g.name = :name')
                ->setParameter('name', 'ORGANISM');

        $countOrganisms = $query->getQuery()->getSingleScalarResult();
        $query->setParameter('name', 'STUDENT');
        $countStudents = $query->getQuery()->getSingleScalarResult();
        $query->setParameter('name', 'INSTRUCTOR');
        $countInstructors = $query->getQuery()->getSingleScalarResult();
        return $this->render('MoocBundle:Default:progressbar.html.twig', array('countCourses' => $countCourses,
                    'countInstructors' => $countInstructors,
                    'countStudents' => $countStudents,
                    'countOrganisms' => $countOrganisms
        )); // passer l'array des cours en param
    }

    public function bestStaffAction() {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('MoocBundle:User');
        $query = $repository->createQueryBuilder('u')
                ->select('u')
                ->orderBy('u.rate','DESC')
                ->setMaxResults(4)
                ->innerJoin('u.groups', 'g')
                ->where('g.name = :name')
                ->setParameter('name', 'INSTRUCTOR');
        
        $bestStaffList = $query->getQuery()->getResult();
        return $this->render('MoocBundle:Default:bestStaff.html.twig', array('bestStaffList' => $bestStaffList));
    }

}
