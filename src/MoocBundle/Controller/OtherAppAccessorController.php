<?php

namespace MoocBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class OtherAppAccessorController extends Controller {

    /**
     * @Route("/checkpassword", name="check_password")
     * @Method({"POST"})
     */
    public function checkPasswordAction(Request $request) {
        $username = $request->get('username', '');
        $password = $request->get('password', '');
        
        $response = new \Symfony\Component\HttpFoundation\Response;
        $response->headers->set("Content-Type", "text/plain");
        
        $user_manager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');

        $user = $user_manager->findUserByUsernameOrEmail($username);
        
        if($user == null){
            $response->setContent("Nom d'utilisateur incorrect");
            return $response;
        }

        $encoder = $factory->getEncoder($user);
        $encodedPwd = $encoder->encodePassword($password, $user->getSalt());

        if($encodedPwd == $user->getPassword()){
            $response->setContent("OK");
        } else {
            $response->setContent("Mot de passe incorrecte");
        }

        return $response;
    }

}
