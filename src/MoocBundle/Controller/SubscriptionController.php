<?php

namespace MoocBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\DateTime;
use MoocBundle\Entity\Subscription;

class SubscriptionController extends Controller {

    public function addAction($course, $student) {

        $em1 = $this->getDoctrine()->getManager();
        $c = $em1->getRepository('MoocBundle:Course')->findOneById($course);
        $em2 = $this->getDoctrine()->getManager();
        $s = $em2->getRepository('MoocBundle:User')->findOneById($student);


        $sub = new Subscription();
        $sub->setCourse($c);
        $sub->setStudent($s);
        $sub->setType('TRAINING');
        $sub->setDate(new \DateTime('now'));
        $em = $this->getDoctrine()->getManager();


        $em->persist($sub);
        $em->flush();

        return $this->redirectToRoute('course_details', array('id' => $course), 301);
    }

    public function deleteAction($course, $student) {

        $em = $this->getDoctrine()->getManager();

        $sub = $em->getRepository('MoocBundle:Subscription')->findOneBy(
                array('type' => 'TRAINING',
                    'course' => $course,
                    'student' => $student)
        );

        if (!$sub) {
            throw $this->createNotFoundException('No guest found');
        }
        $em->remove($sub);
        $em->flush();


        return $this->redirectToRoute('course_details', array('id' => $course), 301);
    }

}
