<?php

namespace MoocBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller {

    public function instructorShowAction($id) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository('MoocBundle:User')->findOneById($id);

        $em1 = $this->getDoctrine()->getManager()->getRepository('MoocBundle:Course');
        $courses = $em1->findByInstructor($id);


        return $this->render('MoocBundle:User:instructorProfile.html.twig', array('instructor' => $result, 'courses' => $courses));
    }

    //profil organisme
    public function organismShowAction($id) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->getRepository('MoocBundle:User')->findOneById($id);



        return $this->render('MoocBundle:User:organismProfile.html.twig', array('organism' => $result));
    }

    // liste des organismes

    public function ListOrganismAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MoocBundle:User')->findByRole("ORGANISM");

        return $this->render('MoocBundle:User:listOrganism.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function MyOrganismListAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MoocBundle:Request')->findByStatus("1");

        return $this->render('MoocBundle:User:MyOrganismList.html.twig', array(
                    'entities' => $entities,
        ));
    }

}
