<?php

namespace MoocBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MoocBundle\Entity\Course;
use MoocBundle\Form\CourseType;

/**
 * Course controller.
 *
 */
class CourseController extends Controller {

    /**
     * Lists all Course entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MoocBundle:Course')->findAll();

        return $this->render('MoocBundle:Course:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Course entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Course();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            var_dump($form->get('Icon'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('course_show', array('id' => $entity->getId())));
        }

        return $this->render('MoocBundle:Course:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Course entity.
     *
     * @param Course $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Course $entity) {
        $form = $this->createForm(new CourseType(), $entity);

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Course entity.
     *
     */
    public function newAction() {
        $entity = new Course();
        $form = $this->createCreateForm($entity);

        return $this->render('MoocBundle:Course:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Course entity.
     *
     */
    public function showAction($id) {

        $em1 = $this->getDoctrine()->getManager();
        $course = $em1->getRepository('MoocBundle:Course')->findOneById($id);
        $chapters = $em1->getRepository('MoocBundle:Chapter')->findBy(array('course' => $id));

        $em1 = $this->getDoctrine()->getManager();
        $subs = $em1->getRepository('MoocBundle:Subscription')->findBy(array('course' => $id));
        return $this->render('MoocBundle:Course:courseDetails.html.twig', array('course' => $course, 'subs' => $subs, 'chapters' => $chapters));
    }

    public function showCourseInstructorAction($id) {

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('ROLE_INSTRUCTOR')) {
            $em = $this->getDoctrine()->getManager();
            $course = $em->getRepository('MoocBundle:Course')->findOneById($id);
            $chapters = $em->getRepository('MoocBundle:Chapter')->findBy(array('course' => $id));

            $subs = $em->getRepository('MoocBundle:Subscription')->findBy(array('course' => $id));
            return $this->render('MoocBundle:Course:courseDetailsInstructor.html.twig', array('course' => $course, 'subs' => $subs, 'chapters' => $chapters));
        } else {


            $this->showAllAction($id);
        }
    }

    /**
     * Displays a form to edit an existing Course entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MoocBundle:Course')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Course entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MoocBundle:Course:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Course entity.
     *
     * @param Course $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Course $entity) {
        $form = $this->createForm(new CourseType(), $entity);

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Course entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MoocBundle:Course')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Course entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('course_edit', array('id' => $id)));
        }

        return $this->render('MoocBundle:Course:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Course entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MoocBundle:Course')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Course entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('course'));
    }

    /**
     * Creates a form to delete a Course entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('course_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function showAllAction() {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em1 = $this->getDoctrine()->getManager();
            $allcourses = $em1->getRepository('MoocBundle:Course')->findAll();
            $allInstructor = $em1->getRepository('MoocBundle:User')->findBy(array('role' => "INSTRUCTOR"));
            $allOrganism = $em1->getRepository('MoocBundle:User')->findBy(array('role' => "ORGANISM"));



            return $this->render('MoocBundle:Course:allCourses.html.twig', array('allCourses' => $allcourses, 'allInstructor' => $allInstructor, 'allOrganism' => $allOrganism));
        }
    }

    public function rechAction() {

        $em1 = $this->getDoctrine()->getManager();
        $title = $_POST['recup'];

        $allcourses = $em1->getRepository('MoocBundle:Course')->recherTitle($title);




        return $this->render('MoocBundle:Course:allCoursesShow.html.twig', array('allCourses' => $allcourses, 'allInstructor' => NULL, 'allOrganism' => NULL));
    }

    public function rechInstructorAction() {

        $em1 = $this->getDoctrine()->getManager();
        $lastname = $_POST['recup'];
        $allIns = $em1->getRepository('MoocBundle:User')->recherIns($lastname, "INSTRUCTOR");



        return $this->render('MoocBundle:Course:allCoursesShow.html.twig', array('allCourses' => NULL, 'allInstructor' => $allIns, 'allOrganism' => NULL));
    }

    public function rechOrganismAction() {

        $em1 = $this->getDoctrine()->getManager();
        $name = $_POST['recup'];
        $allOrg = $em1->getRepository('MoocBundle:User')->recherOrg($name);



        return $this->render('MoocBundle:Course:allCoursesShow.html.twig', array('allCourses' => NULL, 'allInstructor' => NULL, 'allOrganism' => $allOrg));
    }

}
